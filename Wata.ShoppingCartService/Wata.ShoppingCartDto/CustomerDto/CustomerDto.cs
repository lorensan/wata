﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Wata.ShoppingCartMessages
{
    public class CustomerDto
    {
        public string Name { get; set; }

        public CustomerType CustomerType { get; set; }

        public decimal Discount { 
            get
            {
                    decimal result = 0;
                    switch (CustomerType)
                    {
                        case CustomerType.Ocasional:
                            result = 10;
                            break;
                        case CustomerType.Frequent:
                            result = 20;
                            break;
                        case CustomerType.Fidelity:
                            result = 30;
                            break;
                        case CustomerType.HighFidelity:
                            result = 50;
                            break;
                        
                    }
                    return result;
                }
            } 
    }

    public enum CustomerType
    {
        Ocasional,
        Frequent,
        Fidelity,
        HighFidelity
    }
}
