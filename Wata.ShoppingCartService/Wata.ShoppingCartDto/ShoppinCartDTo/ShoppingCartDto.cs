﻿using System;
using System.Collections.Generic;

namespace Wata.ShoppingCartMessages
{
    public class ShoppingCartDto
    {
        public List<ProductDto> Products { get; set; }

        /// <summary>
        /// Discount in percentage
        /// </summary>
        public decimal Discount { get; set; }

        /// <summary>
        /// Total Discount to apply the shopping carts
        /// </summary>
        public decimal TotalDiscount { get; set; }

        /// <summary>
        /// Type of Customer
        /// </summary>
        public CustomerDto Customer { get; set; }
    }
}
