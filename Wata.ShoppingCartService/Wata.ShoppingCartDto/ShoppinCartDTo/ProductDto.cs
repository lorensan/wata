﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Wata.ShoppingCartMessages
{
    public class ProductDto
    {
        public string Name { get; set; }

        public decimal Price { get; set; }

        public decimal Discount { get; set; }

        public GroupType GroupType{ get; set; }

        public decimal GroupDiscount
        {
            get
            {
                decimal result = 0;
                switch (GroupType)
                {
                    case GroupType.Nutrition:
                        result = 10;
                        break;
                    case GroupType.Home:
                        result = 20;
                        break;
                    case GroupType.Electricity:
                        result = 25;
                        break;
                    case GroupType.Informatic:
                        result = 30;
                        break;
                    case GroupType.Clothes:
                        result = 15;
                        break;
                }
                return result;
            }
        }
    }

    public enum GroupType
    {
        Any,
        Nutrition,
        Home,
        Electricity,
        Informatic,
        Clothes
    }
}
