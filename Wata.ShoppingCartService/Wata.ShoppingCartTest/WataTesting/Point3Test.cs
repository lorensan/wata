﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Wata.ShoppingCart;
using Wata.ShoppingCartMessages;

/************************************************************************
 
 3. A discount can be defined for a product group (in the application all items of
the same product group will be considered to apply the discount)
   
************************************************************************/

namespace Wata.ShoppingCartTest
{
    [TestClass]
    public class Point3Test
    {
        [TestMethod]
        [TestCategory("UnitTests")]
        [TestCategory("Point3Test")]
        public void DiscountByProduct_Nominal()
        {
            // Pre-Datas
            DiscountCalculate discountCalculate = new DiscountCalculate();

            ProductDto m_product = new ProductDto()
            {
                Name = "Vileda Mop",
                Discount = 0,
                Price = 10,
                GroupType = GroupType.Home
            };

            // Test
            Assert.IsTrue(discountCalculate.GetPriceGivenProduct(m_product) == 8);
        }

        [TestMethod]
        [TestCategory("UnitTests")]
        [TestCategory("Point3Test")]
        public void DiscountByProduct_WithouDiscount()
        {
            // Pre-Datas
            DiscountCalculate discountCalculate = new DiscountCalculate();

            ProductDto m_product = new ProductDto()
            {
                Name = "Vileda Mop",
                Discount = 0,
                Price = 10,
                GroupType = GroupType.Any
            };

            // Test
            Assert.IsTrue(discountCalculate.GetPriceGivenProduct(m_product) == 10);
        }
    }
}
