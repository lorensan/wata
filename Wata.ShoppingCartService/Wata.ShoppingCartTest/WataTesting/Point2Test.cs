﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Wata.ShoppingCart;
using Wata.ShoppingCartMessages;

/************************************************************************
 
 2. A discount can be defined for a specific product
   
************************************************************************/

namespace Wata.ShoppingCartTest
{

    [TestClass]
    public class Point2Test
    {
        [TestMethod]
        [TestCategory("UnitTests")]
        [TestCategory("Point2Test")]
        public void DiscountByProduct_Nominal()
        {
            // Pre-Datas
            DiscountCalculate discountCalculate = new DiscountCalculate();

            ProductDto m_product = new ProductDto()
            {
                Name = "Wall Clock",
                Discount = 50,
                Price = 10, 
                GroupType = GroupType.Any
            };

            // Test
            Assert.IsTrue(discountCalculate.GetPriceGivenProduct(m_product) == 5);
        }

        [TestMethod]
        [TestCategory("UnitTests")]
        [TestCategory("Point2Test")]
        [ExpectedException(typeof(Exception))]
        public void DiscountByProduct_WhithouPrice()
        {
            // Pre-Datas
            DiscountCalculate discountCalculate = new DiscountCalculate();

            ProductDto m_product = new ProductDto()
            {
                Name = "Wall Clock",
                Discount = 50,
                GroupType = GroupType.Any
            };

            // Test
            Assert.IsTrue(discountCalculate.GetPriceGivenProduct(m_product) == 5);
        }
    }
}
