﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Wata.ShoppingCart;
using Wata.ShoppingCartMessages;

/************************************************************************
 
4. When applying the discounts, the defined rules are applied to products,
product groups and the shopping cart depending on each other (calculation of
the total value of the shopping cart considering all discount types)
   
************************************************************************/

namespace Wata.ShoppingCartTest
{
    [TestClass]
    public class Point5Test
    {
        [TestMethod]
        [TestCategory("UnitTests")]
        [TestCategory("Point3Test")]
        public void TotalAmountByCustomer_Nominal()
        {
            // Pre-Datas
            DiscountCalculate discountCalculate = new DiscountCalculate();
            ShoppingCartDto myShoppingCart = new ShoppingCartDto()
            {
                Products = new List<ProductDto>()
                {
                    new ProductDto()
                    {
                        Name = "Vileda Mop",
                        Discount = 0,
                        Price = 10,
                        GroupType = GroupType.Home
                    },
                    new ProductDto()
                    {
                        Name = "Yatekomo",
                        Discount = 0,
                        Price = 2,
                        GroupType = GroupType.Nutrition
                    },
                    new ProductDto()
                    {
                        Name = "Smart Watch Garmin",
                        Discount = 0,
                        Price = 200,
                        GroupType = GroupType.Informatic
                    }
                },
                Discount = 20,
                Customer = new CustomerDto()
                {
                    Name = "Lorenzo",
                    CustomerType = CustomerType.Ocasional
                }
            };

            // Test
            Assert.IsTrue(discountCalculate.GetTotalShoppingCartPrice(myShoppingCart) > 0);
        }

        [TestMethod]
        [TestCategory("UnitTests")]
        [TestCategory("Point3Test")]
        public void TotalAmountByCustomer_WithouShoppingCartDiscount()
        {
            // Pre-Datas
            DiscountCalculate discountCalculate = new DiscountCalculate();
            ShoppingCartDto myShoppingCart = new ShoppingCartDto()
            {
                Products = new List<ProductDto>()
                {
                    new ProductDto()
                    {
                        Name = "Vileda Mop",
                        Discount = 0,
                        Price = 10,
                        GroupType = GroupType.Home
                    },
                    new ProductDto()
                    {
                        Name = "Yatekomo",
                        Discount = 0,
                        Price = 2,
                        GroupType = GroupType.Nutrition
                    },
                    new ProductDto()
                    {
                        Name = "Smart Watch Garmin",
                        Discount = 0,
                        Price = 200,
                        GroupType = GroupType.Informatic
                    }
                },
                Customer = new CustomerDto()
                {
                    Name = "Lorenzo",
                    CustomerType = CustomerType.Ocasional
                }
            };

            // Test
            Assert.IsTrue(discountCalculate.GetTotalShoppingCartPrice(myShoppingCart) > 0);
        }

        [TestMethod]
        [TestCategory("UnitTests")]
        [TestCategory("Point3Test")]
        [ExpectedException(typeof(Exception))]
        public void TotalAmountByCustomer_WithoutProducts()
        {
            // Pre-Datas
            DiscountCalculate discountCalculate = new DiscountCalculate();
            ShoppingCartDto myShoppingCart = new ShoppingCartDto()
            {
                Discount = 20,
                Customer = new CustomerDto()
                {
                    Name = "Lorenzo",
                    CustomerType = CustomerType.Ocasional
                }
            };

            // Test
            Assert.IsTrue(discountCalculate.GetTotalShoppingCartPrice(myShoppingCart) > 0);
        }
    }
}
