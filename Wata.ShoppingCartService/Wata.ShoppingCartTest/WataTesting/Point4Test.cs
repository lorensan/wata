﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Wata.ShoppingCart;
using Wata.ShoppingCartMessages;

/************************************************************************
 
4. A discount can be applied to the entire shopping cart depending on the
customer group
   
************************************************************************/

namespace Wata.ShoppingCartTest
{
    [TestClass]
    public class Point4Test
    {
        [TestMethod]
        [TestCategory("UnitTests")]
        [TestCategory("Point3Test")]
        public void TotalAmountByCustomer_Nominal()
        {
            // Pre-Datas
            DiscountCalculate discountCalculate = new DiscountCalculate();
            ShoppingCartDto myShoppingCart = new ShoppingCartDto()
            {
                Products = new List<ProductDto>()
                {
                    new ProductDto()
                    {
                        Name = "Vileda Mop",
                        Discount = 0,
                        Price = 10,
                        GroupType = GroupType.Home
                    }
                },
                Customer = new CustomerDto()
                {
                     Name="Lorenzo",
                     CustomerType = CustomerType.HighFidelity
                }
            };
        
            // Test
            Assert.IsTrue(discountCalculate.GetTotalShoppingCartPrice(myShoppingCart) == 4);
        }
    }
}
