﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Wata.ShoppingCart;
using Wata.ShoppingCartMessages;

/************************************************************************
 
 1. A discount can be defined as absolute discount or percentage discount
   
************************************************************************/

namespace Wata.ShoppingCartTest
{
    [TestClass]
    public class Point1Test
    {
        [TestMethod]
        [TestCategory("UnitTests")]
        [TestCategory("Point1Test")]
        public void GetDiscountGivenShoppingCart_DiscountPercentage()
        { 
            // Pre-Datas
            DiscountCalculate discountCalculate = new DiscountCalculate();
            ShoppingCartDto sc = new ShoppingCartDto()
            {
                Products = new List<ProductDto>(),
                Discount = 20
            };

            // Test
            Assert.IsTrue(discountCalculate.GetDiscountGivenShoppingCart(sc) == 20);
        }

        [TestMethod]
        [TestCategory("UnitTests")]
        [TestCategory("Point1Test")]
        public void GetDiscountGivenShoppingCart_TotalDiscount()
        {
            // Pre-Datas
            DiscountCalculate discountCalculate = new DiscountCalculate();
            ShoppingCartDto sc = new ShoppingCartDto()
            {
                Products = new List<ProductDto>(),
                TotalDiscount = 20
            };

            // Test
            Assert.IsTrue(discountCalculate.GetDiscountGivenShoppingCart(sc) == 20);
        }

        [TestMethod]
        [TestCategory("UnitTests")]
        [TestCategory("Point1Test")]
        [ExpectedException(typeof(FormatException))]
        public void GetDiscountGivenShoppingCart_CannotBeBoth()
        {
            // Pre-Datas
            DiscountCalculate discountCalculate = new DiscountCalculate();
            ShoppingCartDto sc = new ShoppingCartDto()
            {
                Products = new List<ProductDto>(),
                TotalDiscount = 20,
                Discount = 15
            };

            // Test
            Assert.IsTrue(discountCalculate.GetDiscountGivenShoppingCart(sc) == 20);
        }
    }
}
