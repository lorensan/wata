﻿using System;
using System.Collections.Generic;
using Wata.ShoppingCartMessages;

namespace Wata.ShoppingCart
{
    public class DiscountCalculate
    {
        /// <summary>
        /// It will return the total discount for the shopping cart
        /// </summary>
        /// <returns></returns>
        public decimal GetDiscountGivenShoppingCart(ShoppingCartDto shoppingCart)
        {
            decimal result = 0;

            if (shoppingCart.Discount >0 && shoppingCart.TotalDiscount >0 )
            {
                throw new FormatException("The discount to apply must to be unique, not both.");
            }
            else
            {
                result = shoppingCart.TotalDiscount > 0 ? shoppingCart.TotalDiscount : shoppingCart.Discount;
            }

            return result;
        }

        /// <summary>
        /// It will return the discount of a product
        /// </summary>
        /// <returns></returns>
        public decimal GetPriceGivenProduct(ProductDto product)
        {
            decimal result = product.Price;

            ValidateProduct(product);

            if (product.Discount > 0)
                result = product.Price - (product.Price * (product.Discount / 100));

            if (product.GroupType != GroupType.Any)
                result = product.Price - (product.Price * (product.GroupDiscount / 100));

            return result;
        }

        /// <summary>
        /// It return the total shoppingCarPrice
        /// </summary>
        /// <param name="shoppingCart"></param>
        /// <returns></returns>
        public decimal GetTotalShoppingCartPrice(ShoppingCartDto shoppingCart)
        {
            decimal priceOfProductsAmout = 0;
            decimal totalShoppingCartPrice = 0;

            ValidateShoppingCarts(shoppingCart);

            // We're going to sum all prices by product with its group type discount
            foreach (ProductDto product in shoppingCart.Products)
            {
                priceOfProductsAmout += GetPriceGivenProduct(product);
            }

            // Now, we 're going to calculate the discount by  shopping car
            totalShoppingCartPrice = priceOfProductsAmout;
            if (shoppingCart.Discount > 0)
            {
                totalShoppingCartPrice = priceOfProductsAmout - (priceOfProductsAmout * (shoppingCart.Discount/ 100));
            }

            // And, at end, we'll apply the discount by type of customer
            if (shoppingCart.Customer != null)
            {
                totalShoppingCartPrice = totalShoppingCartPrice - (totalShoppingCartPrice * (shoppingCart.Customer.Discount / 100));
            }

            return totalShoppingCartPrice;
        }

        #region Validate

        private void ValidateProduct(ProductDto product)
        {
            if (product == null)
                throw new Exception("The object is empty.");

            if (product.Price == 0)
                throw new Exception("The value cannot be 0.");
        }

        private void ValidateShoppingCarts(ShoppingCartDto shoppingCart)
        {
            if (shoppingCart.Products == null)
                throw new Exception("The shopping carts not initilized.");
            
            if (shoppingCart.Products.Count == 0)
                throw new Exception("The shopping carts can't be empty.");
        }

        #endregion
    }
}
